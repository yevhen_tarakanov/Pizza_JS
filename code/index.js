import {pizzaSelectSize, pizzaSelectTopping, show, validate} from "./functions.js";
import {pizzaUser} from "./data-pizza.js"

//const [...inputs] = document.querySelectorAll("#pizza input");
/*
inputs.forEach((input)=>{
    input.addEventListener("click", ()=>{
        console.log("+")
    })
})
*/

document.querySelectorAll(".grid input")
    .forEach((input) => {
        if (input.type === "text" || input.type === "tel" || input.type === "email") {
            input.addEventListener("change", () => {
                if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "email" && validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else {
                    input.classList.add("error")
                }
            })
        } else if (input.type === "reset") {
            input.addEventListener("click", () => {

            })
        } else if (input.type === "button") {
            input.addEventListener("click", () => {
                localStorage.userInfo = JSON.stringify(pizzaUser);
            })
        }
    })

function selectInput(input, data) {
    input.className = ""
    input.classList.add("success")
    data.userName = input.value
}


document.querySelector("#pizza")
    .addEventListener("click", pizzaSelectSize)

// document.querySelector(".ingridients")
//     .addEventListener("click", pizzaSelectTopping)

window.onload = function () {
    let draggable = document.querySelectorAll(".draggable");
    let dragged;
    draggable.forEach((item, index) => {
        item.addEventListener('dragstart', function (evt) {
            this.style.border = "3px dotted #000";
            this.style.borderRadius = "20px";
            dragged = evt;
        }, false)
    })
    draggable.forEach((item) => {
        item.addEventListener("dragend", function (evt) {
            this.style.border = "";
        }, false)
    })
    let table = document.querySelector(".table");
    table.addEventListener("dragenter", function (evt) {
        this.style.border = "3px solid red";
        this.style.borderRadius = "30px";
    }, false);

    table.addEventListener("dragover", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        return false;
    }, false);
    table.addEventListener("drop", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        pizzaSelectTopping(dragged);
        this.style.border = "";
    }, false);

}

show(pizzaUser);


