import {pizzaBD, pizzaUser} from "./data-pizza.js";

const table = document.querySelector(".table")

const pizzaSelectSize = (e) => {
    let sizeMem = pizzaUser.size.price
    if (e.target.tagName === "INPUT" && e.target.checked) {
            pizzaUser.size = pizzaBD.size.find(el => el.name === e.target.id);
        // pizzaBD.size.forEach((el)=>{
        //     if(e.target.id === el.name){
        //     }
        // })
    }
    show(pizzaUser, '',e.target.checked,sizeMem)
    remove();
    removeSauce();
};


const pizzaSelectTopping = (e) => {
    let toppingSpan = document.querySelectorAll('.topping')

    if (pizzaUser.topping.length === 0) {
        swit(e);
    } else {
        let bolSpan = false, bolPizUse;
        toppingSpan.forEach((el) => {
            if (el.id === e.target.id) {
                bolSpan = true;
            }
        })

        for (let i = 0; i < pizzaUser.topping.length; i++) {
            bolPizUse = pizzaUser.topping[i].name.includes(e.target.id)
            if (bolPizUse === true && bolSpan === true) {
                pizzaUser.topping[i].counter = pizzaUser.topping[i].counter + 1;
                showCoun(pizzaUser.topping[i])
                add(pizzaUser);
                break;
            } else if (bolPizUse === false && bolSpan === false) {
                swit(e);
                break;
            }
        }
    }
    img(e);
    remove();
    removeSauce();
}


function swit(e) {
    switch (e.target.id) {
        case "sauceClassic" :
            pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        case "sauceBBQ" :
            pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        case "sauceRikotta" :
            pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        case "moc1" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
        case "moc2" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
        case "moc3" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
        case "telya" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
        case "vetch1" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
        case "vetch2" :
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            break;
    }
    show(pizzaUser, e.target.id);
}


function img(e) {
    if (e.target.tagName === "IMG") {
        if (e.target.id === "sauceClassic" || e.target.id === "sauceBBQ" || e.target.id === "sauceRikotta") {
            table.insertAdjacentHTML("beforeend", `<img class="imgPizzaSauce" id="${e.target.id}" src="${e.target.src}" alt="Error">`);
        } else {
            table.insertAdjacentHTML("beforeend", `<img class="imgPizza" id="${e.target.id}" src="${e.target.src}" alt="Error">`);
        }
    }
}

function showCoun(pizza) {
    const price = document.getElementById("price");
    let totalPrice = pizzaUser.price;
    totalPrice += pizza.price;
    price.innerText = totalPrice;
    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()
}



function show(pizza, id,checked,sizeMem) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping")
    let totalPrice;
    
    if (pizza.topping.length <= 1) {
        totalPrice = 0;
        if (pizza.size !== "") {
            totalPrice += parseFloat(pizza.size.price);
        }
        if (pizza.sauce !== "") {
            totalPrice += parseFloat(pizza.sauce.price);
        }
        if (pizza.topping.length) {
            totalPrice += pizza.topping.reduce((a, b) => a + b.price, 0)
        }
        price.innerText = totalPrice;
        if (pizza.sauce !== "") {
            sauce.innerHTML = `<span class="toppingSauce">${pizza.sauce.productName} <button class="btnDeleteSauce">\u274C</button> </span>`;
        }
        if (Array.isArray(pizza.topping)) {
            topping.innerHTML = pizza.topping.map(el => `<span class="topping" id="${el.name}">${el.productName}
                                                         <span class="counter">${el.counter}</span>
                                                      <button class="btnDelete">\u274C</button> </span>`).join("");
        }
    }
    else {
        totalPrice = pizzaUser.price;

        if(checked){
            if (pizza.size !== "") {
                totalPrice -= sizeMem ;
                totalPrice += parseFloat(pizza.size.price);
                
            }
        }
        
        let toppingSauce = Boolean(document.querySelector('.toppingSauce'))
        if (toppingSauce == false) {
            if (pizza.sauce !== "") {
                totalPrice += parseFloat(pizza.sauce.price);
                sauce.innerHTML = `<span class="toppingSauce">${pizza.sauce.productName} <button class="btnDeleteSauce">\u274C</button> </span>`;
            }
        }
        if (pizza.topping.length) {
            pizza.topping.forEach((el, index) => {
                if (id == el.name) {
                    totalPrice += pizza.topping[index].price;
                }
            })
        }
        price.innerText = totalPrice;

        if (Array.isArray(pizza.topping)) {
            topping.innerHTML = pizza.topping.map(el => `<span class="topping" id="${el.name}">${el.productName}
                                                         <span class="counter">${el.counter}</span>
                                                      <button class="btnDelete">\u274C</button> </span>`).join("");
        }
    }
    
    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()
}

function add(pizza) {
    const topping = document.getElementById("topping")
    let toppingSpan = document.querySelectorAll('.topping')
    toppingSpan.forEach(el => {
        el.remove();
    })
    topping.innerHTML = pizza.topping.map(el => `<span class="topping" id="${el.name}">${el.productName}
                                                         <span class="counter">${el.counter}</span>
                                                      <button class="btnDelete">\u274C</button> </span>`).join("");
}


function removeSauce() {
    let btnDeleteSauce = document.querySelector(".btnDeleteSauce");
    const price = document.getElementById("price");
    if (btnDeleteSauce) {
        btnDeleteSauce.addEventListener("click", function () {
            let toppingSauce = document.querySelector(".toppingSauce");
            let totalPrice = pizzaUser.price;
            if (pizzaUser.sauce !== "") {
                totalPrice -= parseFloat(pizzaUser.sauce.price);
            }
            pizzaUser.price = totalPrice;

            price.innerText = totalPrice;
            if(pizzaUser.sauce != ""){
                toppingSauce.remove();
            }
            pizzaUser.sauce = '';
            let imgPizza = document.querySelectorAll(".imgPizzaSauce");
            imgPizza.forEach(item => {
                item.remove();
            })
        })

    }
}

function remove() {
    let btnDelete = document.querySelectorAll(".btnDelete");
    if (btnDelete) {

        btnDelete.forEach((item, index) => {
            item.addEventListener('click', event => {
                let idElem = item.parentNode.id;
                let pizzaBDPrice = '';
                let totalPrice = pizzaUser.price;
                let pizzaBDTopping = pizzaBD.topping;
                const price = document.getElementById("price");

                for (let i = 0; i < pizzaUser.topping.length; i++) {
                    if (idElem === pizzaUser.topping[i].name && pizzaUser.topping[i].counter > 1) {
                        for (let i = 0; i < pizzaBDTopping.length; i++) {
                            if (idElem == pizzaBDTopping[i].name) {
                                pizzaBDPrice = pizzaBDTopping[i].price;
                            }
                        }
                        totalPrice -= pizzaBDPrice;
                        pizzaUser.price = totalPrice;
                        price.innerText = totalPrice;

                        pizzaUser.topping[i].counter -= 1;
                        let toppingSpan = document.querySelectorAll('.topping')
                        const topping = document.getElementById("topping")
                        toppingSpan.forEach(el => {
                            el.remove();
                        })
                        topping.innerHTML = pizzaUser.topping.map(el => `<span class="topping" id="${el.name}">${el.productName}
                                                         <span class="counter">${el.counter}</span>
                                                      <button class="btnDelete">\u274C</button> </span>`).join("");
                    } else if (idElem === pizzaUser.topping[i].name && pizzaUser.topping[i].counter == '1') {
                        for (let i = 0; i < pizzaBDTopping.length; i++) {
                            if (idElem == pizzaBDTopping[i].name) {
                                pizzaBDPrice = pizzaBDTopping[i].price;

                            }
                        }
                        totalPrice -= pizzaBDPrice;
                        pizzaUser.price = totalPrice;
                        price.innerText = totalPrice;

                        let imgPizza = document.querySelectorAll(".imgPizza");
                        imgPizza.forEach(item => {
                            if (idElem == item.id) {
                                table.removeChild(item);
                            }
                        });
                        if (pizzaUser.topping[i].name == idElem) {
                            pizzaUser.topping.splice(i, 1)
                        }
                        item.parentNode.remove(index);
                        if (pizzaUser.topping == "") {
                            topping.innerHTML = pizzaUser.topping.map(el => `<span class="topping" id="${el.name}">${el.productName}
                                                         <span class="counter">${el.counter}</span>
                                                      <button class="btnDelete">\u274C</button> </span>`).join("");
                        }
                    }
                }
                return remove();
            })
        })
    }
}


const validate = (pattern, value) => pattern.test(value);

export {pizzaSelectSize, pizzaSelectTopping, show, validate}